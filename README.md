# AWS EFS mount

## Description
This image contains AWS EFS tools. https://github.com/aws/efs-utils

The image is based on the official ubuntu image.

This image force to use EFS with TLS (with stunnel) because we are using IAM authentication (More details about in [transit encryption](https://docs.aws.amazon.com/efs/latest/ug/encryption-in-transit.html) and [IAM authentication](https://docs.aws.amazon.com/efs/latest/ug/auth-and-access-control.html))

This image supports [sd_notify(3)](https://www.freedesktop.org/software/systemd/man/sd_notify.html).

## Tags
We push a `latest` tag on this repository, to run an older version please check out the different tags.

## Usage
This image mount an EFS inside `/mnt/efs`.

This image needs aws credentials that must be passed as file mount to `/root/.aws/`

This image need the docker capacity [`CAP_SYS_ADMIN`](https://man7.org/linux/man-pages/man7/capabilities.7.html)

To be able to interact with your mount point outside the docker container, you must set the [bind-propagation](https://docs.docker.com/storage/bind-mounts/#configure-bind-propagation) to `shared`

Some extra options can be passed using those environment variables:

| Name | Description                                                                                              | Default value |
| --- |----------------------------------------------------------------------------------------------------------| --- |
| `AWS_REGION` | Region of the EFS                                                                                        | `us-east-1` |
| `EFS_MOUNT_DNS_SUFFIX` | Change the DNS suffix of EFS url ([more information here]()) (useful when using EFS outside AWS VPC)     | `amazonaws.com` |
| `EFS_MOUNT_STUNNEL_CHECK_CERT_HOSTNAME` | Enable stunnel certificate hostname (This might be useful if you use custom DNS endpoint)                | `true` |
| `AWS_PROFILE` | AWS [named profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) to use | Not defined |
| `EFS_MOUNT_NFS_EXTRA_OPTIONS` | Other option to add to the EFS mount                                                                     | Not defined |
| `NOTIFY_SOCKET` | Path to systemd notify socket where to notify mount is ready                                             | Not defined |

For a simple usage in AWS VPC with EC2 role:

```shell
sudo podman run -it --rm -d \
  --cap-add=CAP_SYS_ADMIN \
  -v "$(pwd)/efs":/mnt/efs:shared \
  registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest fs-1234567890abcdefg.efs.us-east-1.amazonaws.com
```

For usage outside AWS with endpoint IP

```shell
sudo podman run -it --rm -d \
  --cap-add=CAP_SYS_ADMIN \
  -e "EFS_MOUNT_ENDPOINT_IP=172.16.0.2" \
  -v "${HOME}/.aws/credentials":/root/.aws/credentials \
  -v "${HOME}/.aws/config":/root/.aws/config \
  -v "$(pwd)/mountpoint":/mnt/efs:shared \
  registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest fs-1234567890abcdefg.efs.us-east-1.amazonaws.com
```

For usage outside AWS with custom DNS name
```shell
sudo podman run -it --rm -d \
  --cap-add=CAP_SYS_ADMIN \
  -v "${HOME}/.aws/credentials":/root/.aws/credentials \
  -v "${HOME}/.aws/config":/root/.aws/config \
  -e "EFS_MOUNT_DNS_SUFFIX=example.com" \
  -e "EFS_MOUNT_STUNNEL_CHECK_CERT_HOSTNAME=false" \
  -v "$(pwd)/mountpoint":/mnt/efs:shared
  registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest fs-1234567890abcdefg.efs.us-east-1.example.com
```

## Notification

This container support [sd_notify(3)](https://www.freedesktop.org/software/systemd/man/sd_notify.html).

When environment variable `NOTIFY_SOCKET` is set, container will send signal `READY=1` to `NOTIFY_SOCKET`

This can be useful when using in a systemd unit.

Here is an example of systemd unit using podman and service type notify:

```bash
[Unit]
Description=Mount EFS
After=network-online.target
After=local-fs.target
After=network-online.target

[Service]
Restart=on-failure
Type=notify
KillMode=none
ExecStartPre=-mkdir -p /mnt/efs
ExecStartPre=-podman stop efs
ExecStartPre=podman pull registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest
ExecStart=podman run -d --rm --sdnotify=container --cap-add=CAP_SYS_ADMIN --name efs -v "/mnt/efs":/mnt/efs:shared registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest fs-1234567890abcdefg.efs.us-east-1.example.com
ExecStop=podman stop efs
Environment=NOTIFY_SOCKET=/run/systemd/notify

[Install]
WantedBy=multi-user.target
```

## Use DNS TXT record

Because of AWS EFS tools limitations, custom EFS DNS record must match `{az}.{fs_id}.efs.{region}.{dns_name_suffix}
`. The direct limitation is the resilience. If EFS ID change, we must the CNAME.

To avoid that, you can pass a DNS TXT record instead of CNAME or A record.

Valid keys are:

 * `efs-uri`: record that match the EFS util regex
 * `region`: region of the EFS resource
 * `dns-suffix`: suffix of your `efs-uri` DNS record (i.e for `fs-xxxxxxx.efs.us-east-1.example.com`, `dns-suffix` will be `example.com`)

## Limitations

Authentication methods supported by EFS tools are:
 * instance security credentials service (IMDS)
 * credentials files
 * ECS credentials relative uri

Docker network driver must be set to `host` because EFS over tls use stunnel and loopback address. (Podman don't need it)

See https://github.com/aws/efs-utils/issues/110

sd_notify STATUS not supported (see https://github.com/containers/podman/issues/12636)

## Labels
We set labels on our images with additional information on the image.
We follow the guidelines defined at http://label-schema.org/.
Visit their website for more information about those labels.

## Contributions
Contributions are welcome.
