1.4.0
====

* feat: Add docker healthcheck
* fix: run efs watchdog manually
* maintenance: pin EFS utils to `1.31.3`
* chore: allow 1 error with dockerlint because of issue with HEALTHCHECK
* chore: remove git protocol for pre-commit hook
* chore: bump pre-commit hook to `4.1.0`

1.3.0
====

* feat: add DNS TXT record support
* chore: ignore mega-lint yaml_vbr plugin

1.2.0
====

* feat: add `SIGRTMIN+3` as standard stop signal (used by `podman` as standard stop signal with systemd)
* refactor: rename entrypoint to match with `podman` systemd standard
* fix: return `fusermount` exit code instead of signal code

1.1.0
====

* feat: Add systemd notification when EFS in ready
* doc: Fix repository URL

1.0.0
=====

* feat: initial version

0.0.0
=====

* tech: initial version
