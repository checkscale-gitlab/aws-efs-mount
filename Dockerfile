ARG BASE_VERSION=21.04
ARG EFS_UTILS_VERSION=1.31.3

FROM docker.io/library/ubuntu:$BASE_VERSION AS builder

ARG EFS_UTILS_VERSION
ENV EFS_UTILS_VERSION=$EFS_UTILS_VERSION

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

COPY resources/builder /resources

RUN /resources/build && rm -rf /resources


FROM docker.io/library/ubuntu:$BASE_VERSION

ARG EFS_UTILS_VERSION

ENV EFS_UTILS_VERSION=$EFS_UTILS_VERSION

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

VOLUME /data

COPY resources/final /resources
COPY --from=builder /efs-utils-$EFS_UTILS_VERSION/build/*.deb /resources/efs-utils.deb

RUN /resources/build && rm -rf /resources

WORKDIR /data

HEALTHCHECK --interval=10s --timeout=5s --start-period=60s --retries=3 CMD /usr/local/bin/healthcheck || exit 1

ENTRYPOINT ["/usr/local/sbin/init"]

LABEL "maintainer"="WildBeavers" \
      "org.label-schema.name"="aws-cli" \
      "org.label-schema.base-image.name"="docker.io/library/ubuntu" \
      "org.label-schema.base-image.version"="$BASE_VERSION" \
      "org.label-schema.description"="Amazon EFS tools in a container" \
      "org.label-schema.url"="https://github.com/aws/efs-utils" \
      "org.label-schema.vcs-url"="https://gitlab.com/wild-beavers/docker/aws-efs-mount" \
      "org.label-schema.vendor"="WildBeavers" \
      "org.label-schema.schema-version"="1.0.0-rc.1" \
      "org.label-schema.applications.amazon-efs-utils.version"="$EFS_UTILS_VERSION" \
      "org.label-schema.vcs-ref"=$VCS_REF \
      "org.label-schema.version"=$VERSION \
      "org.label-schema.build-date"=$BUILD_DATE \
      "org.label-schema.usage"="See README.md"
